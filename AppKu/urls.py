from django.urls import path
from . import views

app_name = 'AppKu'

urlpatterns = [
    path('', views.home,name='home'),
    path('profile/', views.profile, name='profile'),
    path('contact/', views.contact, name='contact'),
    path('seeMore/', views.seeMore, name='seeMore'),
    path('schedule/', views.schedule, name='schedule'),
    path('schedule/create', views.schedule_create, name='schedule_create'),
    path('schedule/clear/<int:id>/', views.schedule_delete, name='schedule_delete'),

    ]
