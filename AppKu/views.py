from django.shortcuts import render
from . import models
from.models import Schedule
from . import forms
from django.shortcuts import redirect
from django.http import HttpResponse

# Create your views here.
def home(request):
    return render(request, 'home.html')

def profile(request):
    return render(request, 'profile.html')

def contact(request):
    return render(request, 'contact.html')

def seeMore(request):
    return render(request, 'seeMore.html')

def schedule(request):
    schedules = Schedule.objects.all().order_by('date')
    return render(request, 'schedule.html', {'schedules': schedules})

def schedule_create(request):
    if request.method == 'POST':
        form = forms.ScheduleForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/schedule')

    else:
        form = forms.ScheduleForm()
    return render(request, 'schedule_create.html', {'form': form})

def schedule_delete(request, id):
    if request.method == 'POST':
        Schedule.objects.filter(id=id).delete()
        return redirect('AppKu:schedule')
    else:
        return HttpResponse("/GET not allowed")


